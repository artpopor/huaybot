from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import undetected_chromedriver as uc

CHROMEDRIVER_PATH = '/usr/bin/chromedriver' #for ubantu server
#CHROMEDRIVER_PATH = 'C:\\Windows\webdriver\\chromedriver.exe' #for windows


options = Options()
options.add_argument("--headless")
options.add_argument("--window-size=1920,1080")
options.add_argument('--no-sandbox')
options.add_argument("--user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.517 Safari/537.36")
driver = webdriver.Chrome(executable_path=CHROMEDRIVER_PATH,options=options)

driver.get("https://www.huay.com/login")
driver.save_screenshot('browser.png')
print(driver.title)
driver.close()
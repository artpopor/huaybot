from selenium import webdriver
import selenium.webdriver.support.ui as ui
from datetime import datetime
import pytz
import time
from pyvirtualdisplay import Display
from selenium.webdriver.chrome.options import Options


while True :
    #--------------------TEXT---------------------
    textfile = open("normal_record.txt","a")
    textfileVIP = open("VIP_record.txt","a")
    #--------------CHROME------------------
    options = webdriver.ChromeOptions()
    options.add_argument("--window-size=1920,1080")
    options.add_argument("--headless")
    options.add_argument("--user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36")
    #driver = webdriver.Chrome(executable_path="C:\\Windows\webdriver\\chromedriver.exe",options=options)
    driver = webdriver.Chrome(executable_path="C:\\Windows\webdriver\\chromedriver.exe")
    #--------------------------------------
   
    #---------Process 1----------------
    driver.get("https://www.huay.com/login")
    localtime = datetime.today().ctime()
    lastest_rec = driver.find_element_by_css_selector('#yeekee > div > div > div > div.mb-2.yeekee-last-result.type-yeekeehuay')
    new_rec = lastest_rec.text.split()
    rec_round = new_rec[3] 
    up_number = new_rec[5]
    down_number = new_rec[7]
    textfile.writelines("\n"+rec_round+" : "+ up_number +" : "+ down_number+" | "+str(localtime))
    textfile.close()
    time.sleep(2)

    #---------Process 2----------------
    button = driver.find_element_by_css_selector('#yeekee > div > div > div > div.row.no-gutters.result-yeekee-nav-tab > div:nth-child(2)')
    button.click()
    VIP_lastest_rec = driver.find_element_by_css_selector('#yeekee > div > div > div > div.mb-2.yeekee-last-result.type-yeekeehuay > div')
    new_rec_VIP = VIP_lastest_rec.text.split()
    round_VIP = new_rec_VIP[4]
    up_number_VIP = new_rec_VIP[6]
    down_number_VIP  = new_rec_VIP[8]
    textfileVIP.writelines("\n"+round_VIP+" : "+ up_number_VIP +" : "+ down_number_VIP+" | "+str(localtime))
    textfileVIP.close()
    driver.close()
    #-----Timer-------
    time.sleep(10)
